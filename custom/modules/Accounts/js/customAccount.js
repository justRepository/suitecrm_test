$( document ).ready(function(){

  var beforeupdatestatus = $("div[field=salestatus").text(); // Получаем значение статуса до изменения

    $(document).on('change', '#salestatus', function() { // Ловим изменения в селекте 
        $('#div_note_salestatus').remove();
        $('#hidden_note_salestatus').empty();
        $('#inline_edit_field').append('<div id="div_note_salestatus"><p><b>Факт-причина для изменения SaleStatus</b></p><textarea id="note_description" name="note_description" rows="6" cols="75" title="" tabindex="0"></textarea><div id="hidden_note_salestatus" style="display:none;"></div></div>');
    });

    $('#hidden_note_salestatus').keypress(function(){
      var Value = $('#hidden_note_salestatus').val();
      $('#hidden_note_salestatus').text(Value);
    });

    $(document).on('click', '#inlineEditSaveButton', function() {

      var description = $("#hidden_note_salestatus").text();
      var afterupdatestatus = $("#salestatus option:selected").text(); // Получаем значение статуса после изменения
      var name = 'SaleStatus UPDATE: ' + beforeupdatestatus + '->' + afterupdatestatus;
      var parent_id = $("input[name=record]").val(); // Получаем id родительской записи
      
      alert('ok5');

      $.ajax({
       method: 'POST',
       url: 'index.php',
       dataType: 'json',
       data: {
        module: 'Accounts',
        action: 'notesalestatus',
        name: name,
        parent_id: parent_id,
        description: description,
      }
   //  success: function(){

   //  },
   // error: function(){

   // }
      });
    });
});