<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class AccountsController extends SugarController
{
	public function action_notesalestatus(){
		$noteBean = BeanFactory::newBean('Notes');

		$noteBean->assigned_user_id = $GLOBALS['current_user']->id;
		$noteBean->name = $_POST['name'];
		$noteBean->parent_type = 'Accounts';
		$noteBean->parent_id = $_POST['parent_id'];
		$noteBean->description = $_POST['description'];

		$noteBean->save();
	}
}
