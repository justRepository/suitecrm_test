<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.02.2019
 * Time: 23:17
 */

$dictionary['Account']['fields']['salestatus'] =
    array (
        'required' => false,
        'name' => 'salestatus',
        'vname' => 'LBL_SALESTATUS',
        'type' => 'enum',
        'massupdate' => 0,
        'default' => '1',
        'no_default' => false,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => true,
        'inline_edit' => true,
        'reportable' => true,
        'unified_search' => false,
        'merge_filter' => 'disabled',
        'len' => 100,
        'size' => '20',
        'options' => 'SaleStatus',
        'studio' => 'visible',
        'dependency' => false,
    );